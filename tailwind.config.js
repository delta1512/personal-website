/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./*.html"],
  theme: {
    minHeight : {
      'feature-content' : '500px',
      'footer-canvas' : '275px'
    },
    maxHeight : {
      'feature-content' : '500px',
      'footer-canvas' : '275px'
    },
    extend: {
      fontFamily : {
        'poppins' : ['Poppins', 'sans-serif']
      },
      backgroundImage : {
        'landing-img' : "url('/res/img/marcus_belcastro_silhouette_portrait.webp')"
      },
      backgroundColor : {
        'yora-green' : '#137547',
        'yora-green-acc' : '#2AA24F',
        'yora-yellow' : '#FCDE16',
        'yora-grey' : '#151D20',
        'buzz-red' : '#E52133',
        'wss-blue' : '#1D5BE5'
      }
    },
  },
  plugins: [],
}
